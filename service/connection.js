var mongo = require("mongodb").MongoClient;
var express = require("express");
var app = express();
var jsonObj = require("json-stringify");
var routers = require("./mongoUtil");
var bodyParser = require("body-parser");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use("/source", express.static("../" + '/source'));

app.use(routers);


app.listen(3022, function(){
    console.log('Connected with server');
});