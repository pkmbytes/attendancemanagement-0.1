var express = require("express");
var bodyParser = require("body-parser");
var app = express();

app.use(express.json());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.get("/", renderDefaultPage);
app.get("/login", renderDefaultPage);
app.get("/error", renderErrorPage);

app.post("/loaduser", function(req, res){

    console.log((req.body ? req.body : "No Body"));
    if(req.body.username == "test" && req.body.password =="test")
    {
        renderIndexPage(req, res);
    }
    else
    {
		var errorMsg = encodeURIComponent('User Not Found');
		 res.redirect('/error?errorMsg='+errorMsg);
    }

 });
 
 function renderErrorPage(req, res){
	app.use("/source", express.static("../" + '/source'));
	res.sendFile('error.html', { root: "../" } );
 }

function renderDefaultPage(request, response)
{

    app.use("/source", express.static("../" + '/source'));
    response.sendFile('login.html', { root: "../" } );

}

function renderIndexPage(request, response)
{

    app.use("/source", express.static("../" + '/source'));
    response.sendFile('index.html', { root: "../" } );

}
app.listen("3020");