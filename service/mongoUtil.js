var mongoClient = require("mongodb").MongoClient;
var express = require("express");
var router = express.Router();
var bodyParser = require("body-parser");
var expressObj = express();
var dbName = "test";
var db;

mongoClient.connect("mongodb://localhost:27017/" + dbName, function(error, database){
    if(error)
    {
        throw error
    }
    else
    {
        db = database;
    }
});

router.post("/loaduser", function(request, response){
    var username = request.body.username;
    var password = request.body.password;
    var userFind = false;
    db.collection("users").find().toArray(function(error, result){
       result.forEach(function(element){
           if(element._id == username && element.password == password)
           {
               userFind = true;
               renderDefaultPage(request, response);
           }
       });

       if(!userFind)
       {
           response.send({
               "responseMessage" : "User Not Found",
               "responseCode" : 402
           });
       }
    })
});

router.get("/login", function(request, response){
    renderLoginPage(request, response);
});


function renderDefaultPage(request, response)
{
    // expressObj.use("/source", express.static("../" + '/source'));
    response.sendFile('index.html', { root: "../" } );
}

function renderLoginPage(request, response)
{
    // expressObj.use("/source", express.static('../source'));
    response.sendFile('login.html', { root: "../" } );
}


module.exports = router;