function renderTile(config)
{
    var defaultSettings = {};
    this.config = $.extend(true, config, defaultSettings);
    
    this.render();
}

renderTile.prototype.render = function()
{
    var _this = this;
    var htmlElem = [];
    var yearDetails = _this.config.details.years;
    htmlElem.push('<div class="yearCont">');
        for(var index = 0; index < yearDetails.length; index++)
        {
            htmlElem.push('<div class="yearDetailsCont">');
                htmlElem.push('<div class="yearHeadingCont">Year ' + yearDetails[index].year + '</div>');
                htmlElem.push('<div class="tileListContainer">');
                for(var i = 0; i < yearDetails[index].classDetails.length; i ++)
                {
                    var classDetails = yearDetails[index].classDetails[i];
                    htmlElem.push('<div class="tileContainer">');
                        htmlElem.push('<div class="tileHeader">');
                            htmlElem.push('<div class="sectionName">Class  ' + classDetails.sectionName + '</div>');
                        htmlElem.push('</div>');
                        htmlElem.push('<div class="tileBody">');
                            htmlElem.push('<div class="classStrength">' + classDetails.strength + ' </div>');
                        htmlElem.push('</div>');
                        htmlElem.push('<div class="tileFooter">');
                            htmlElem.push('<div class="sectionEnterBtn attendanceBtn">Attendance</div>');
                            htmlElem.push('<div class="sectionEnterBtn">Mark</div>');
                        htmlElem.push('</div>');
                    htmlElem.push('</div>');
                }
                htmlElem.push('</div>');
            htmlElem.push('</div>');
        }
    htmlElem.push('</div>')
    

    $(_this.config.renderElement).append(htmlElem.join(''));
    _this.bindEvents();
}

renderTile.prototype.bindEvents = function()
{
    $(".tileContainer").off("mouseover").on("mouseover", function(){
       $(this).addClass("setZoom");
    });
    $(".tileContainer").off("mouseout").on("mouseout", function(){
        $(this).removeClass("setZoom");
    });
};