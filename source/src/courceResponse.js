var response = {
    "responseMessage" : "Success",
    "responseCode" : 200,
    "departmentDetails" : {
        "departmentName" : "MCA",
        "departmentId" : "2013MCA",
        "years" : [
            {
                "year" : "1",
                "classDetails" : [
                    {
                        "sectionName" : "A",
                        "strength" : 60
                    },
                    {
                        "sectionName" : "B",
                        "strength" : 55
                    },
                    {
                        "sectionName" : "C",
                        "strength" : 50
                    }
                ]
        
            },
            {
                "year" : "2",
                "classDetails" : [
                    {
                        "sectionName" : "A",
                        "strength" : 52
                    },
                     {
                        "sectionName" : "B",
                        "strength" : 48
                    },
                ]
            },
            {
                "year" : 3,
                "classDetails" : [
                    {
                        "sectionName" : "A",
                        "strength" : 46
                    },
                ]
            },
            {
                "year" : "4",
                "classDetails" : [
                    {
                        "sectionName" : "A",
                        "strength" : 60
                    },
                    {
                        "sectionName" : "B",
                        "strength" : 55
                    },
                    {
                        "sectionName" : "C",
                        "strength" : 50
                    },
                    {
                        "sectionName" : "D",
                        "strength" : 59
                    }
                ]
        
            }
        ]
    }
}
